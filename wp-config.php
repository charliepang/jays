<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wordpress' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'NU2)!@Lj|^}jOxs:ZZszB9-`o+!YLJAO7 L[6&2xpx@#H7@*a!L8JZ{b6&m^-Q+_' );
define( 'SECURE_AUTH_KEY',  '{%i8B!ym0n(JmdeP1=z+wn1f 2[XhqEf6B4#2MG*_9)[fp0==&>=Tf[*q,mw DZ|' );
define( 'LOGGED_IN_KEY',    'U$Cgi:8}|>&PDg{7v_0MvAT$@hp^&c>jhI(2~]E%8uCP3pcc)*XY1v[POfUN8S=|' );
define( 'NONCE_KEY',        ')hDT7n|8duP4:S(M<aWXV~pW?4~,1gDAk7?S=w>p^oB>Rbg{#C/F[6xd+8qV4Msa' );
define( 'AUTH_SALT',        'bqW,[U;mu2B60^>!$+XMtP_9hMOD]VD],MFd,F{AcQRhhYPhOF|{E#TZ3q@uMP<u' );
define( 'SECURE_AUTH_SALT', '/[1;bWE7r3Kx4k5B)Av4wbPi|Xv]8!2 35pj1P:fC+P_Y%10OlZ)V5Jd)+{PgR/_' );
define( 'LOGGED_IN_SALT',   'amHM%nyUoeRk+})KP&<8t: k,[U+Pi3v.}fpDtZAG4^CN}=eyHHb6hlgh^Qgt-^!' );
define( 'NONCE_SALT',       'SbIdY<ch*`*5FFxLrB:]IEce_.xwN*L^/n.y56Xi3GO7X&=Z~EbtWkT]zIg<kH{2' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
