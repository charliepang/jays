<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Nineteen
 * @since 1.0.0
 */

?>

	</div><!-- #content -->
    
    <footer>            
      <div class="footer-main">
          <ul class="ml-auto">
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="#">Home</a>
            </li>
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="#teams">Teams</a>
            </li>
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="mailto:charlie.pang.217@gmail.com">Contact</a>
            </li>
          </ul>
          <p>@ 2019 All rights Reserved</p>
      </div>
      
  </footer>

</div><!-- #page -->

<?php wp_footer(); ?>

	<script src="<?php echo get_template_directory_uri(); ?>/js/jquery-3.4.1.min.js" ></script>
    <script src="<?php echo get_template_directory_uri(); ?>/js/popper.min.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/js/bootstrap.min.js"></script>
    
  	<script src="<?php echo get_template_directory_uri(); ?>/js/jays.js"></script>
        
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/jquery-ui.css">
    <script src="<?php echo get_template_directory_uri(); ?>/js/jquery-ui.min.js"></script>
        
    <link href="<?php echo get_template_directory_uri(); ?>/css/jays.css" rel="stylesheet" /> 
    <script src="<?php echo get_template_directory_uri(); ?>/js/jquery.confirm.min.js"></script>
    <?php /*
    <script src="<?php echo get_template_directory_uri(); ?>/js/bootstrap.min.js"></script>
    <link href="<?php echo get_template_directory_uri(); ?>/css/bootstrap.css" rel="stylesheet">
    <link href="<?php echo get_template_directory_uri(); ?>/css/bootstrap-theme.min.css" rel="stylesheet" /> 
    */ ?>
    <script src="<?php echo get_template_directory_uri(); ?>/js/jquery.noty.packaged.min.js"></script>
    <link href="<?php echo get_template_directory_uri(); ?>/css/animate.css" rel="stylesheet" /> 
    
    <script src="<?php echo get_template_directory_uri(); ?>/js/jquery.dataTables.min.js"></script>
    <link href="<?php echo get_template_directory_uri(); ?>/css/jquery.dataTables.min.css" rel="stylesheet" /> 


<?php
/*global $post;
if ($post->ID == 11 ){
	include ('mlb-api/init.php') ;
} else if ($post->ID == 14 ){
	//global $load_page;
	//$load_page = 'teams';
	include ('mlb-api/init.php') ;
}*/
include ('mlb-api/init.php') ;

?>

</body>
</html>
