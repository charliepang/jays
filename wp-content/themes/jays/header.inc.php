<div class="menu-container">
    <!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-light fixed-top mainNav" id="mainNav">
      <div class="container">
        <a class="navbar-logo js-scroll-trigger" href="#"></a>
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          Menu
          <i class="fas fa-bars"></i>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="navbar-nav text-uppercase ml-auto">
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="#">HOME</a>
            </li>
            <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle" href="#teams" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">TEAMS</a>
              <div class="dropdown-menu dropdown-menu-right animate slideIn" aria-labelledby="navbarDropdown">
                  <a class="dropdown-item" href="#teams">All teams</a>
                  <a class="dropdown-item" href="#american_league">American League</a>
                  <a class="dropdown-item" href="#national_league">National League</a>
                </div>                                              
            </li>
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="mailto:charlie.pang.217@gmail.com">CONTACT</a>
            </li>
          </ul>
        </div>
      </div>
    </nav>
</div>