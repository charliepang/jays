

<% if (viewType == "home"){  %> 
    
<% } else if (viewType == "teams"){  %> 
	<div class="container teams-page">
        <div class="row">
          <div class="col-12 col-md-6">
            <p class="subheader animated fadeInDown">American League</p>
            <div id="aleague_list"></div>
          </div>
          
          <div class="col-12 col-md-6">
            <p class="subheader animated fadeInDown">National League</p>
            <div id="nleague_list"></div>
          </div>
        </div><!-- /.row -->
    </div>
    
<% } else if (viewType == "teams_al"){  %> 
	<div class="container teams-page">
        <h2 class="header animated fadeInUp"><span>American League</span></h2>
        <div class="row">
          <div class="col-12 col-md-4">
            <p class="subheader animated fadeInDown">AL East</p>
            <div id="division1"></div>
          </div>
          
          <div class="col-12 col-md-4">
            <p class="subheader animated fadeInDown">AL Central</p>
            <div id="division2"></div>
          </div>
          
          <div class="col-12 col-md-4">
            <p class="subheader animated fadeInDown">AL West</p>
            <div id="division3"></div>
          </div>
        </div><!-- /.row -->
    </div>
    
<% } else if (viewType == "teams_nl"){  %> 
	<div class="container teams-page">
        <h2 class="header animated fadeInUp"><span>National League</span></h2>
        <div class="row">
          <div class="col-12 col-md-4">
            <p class="subheader animated fadeInDown">NL East</p>
            <div id="division1"></div>
          </div>
          
          <div class="col-12 col-md-4">
            <p class="subheader animated fadeInDown">NL Central</p>
            <div id="division2"></div>
          </div>
          
          <div class="col-12 col-md-4">
            <p class="subheader animated fadeInDown">NL West</p>
            <div id="division3"></div>
          </div>
        </div><!-- /.row -->
    </div>
    
<% } else if (viewType == "team"){  %> 
	<div class="container team-page">
        <div class="row">
          <div class="col-lg-3">
            <p class="header">Next Game</p>
            <div id="nextgame_matchup">
        		<div class="row">
                    <div class="col-md-5 col1">
                    </div>
                    <div class="col-md-2 col2">
                        <p class="team_at">@</p>
                    </div>
                    <div class="col-md-5 col3">
                        <p>Home</p>
                        <div class="home_logo"></div>
                        <div class="home_team"></div>
                        <div class="home_record"></div>
                    </div>
                </div>
            </div>
          </div>
          
          <div class="col-lg-9">
            <p class="header">Team Leader</p>
            <div id="team_leaders"></div>
          </div>
        </div><!-- /.row -->
        <div class="row">
          <div class="col-md-12">
            <p class="page-header2222"></p>
        	<h2 class="header animated fadeInUp"><span class="page-header">American League</span></h2>
            <div class="tabMain">
                <div class="navWrap">
                    <ul class="clearfix">
                        <li><a href="#" id="nav_active" class="tab_current"><span class="line"><span>Active</span></span></a></li>
                        <li><a href="#" id="nav_40" class=""><span class="line"><span>40 Man</span></span></a></li>
                        <li><a href="#" id="nav_coaches" class=""><span class="line noline"><span>Coaches</span></span></a></li>
                    </ul>
                </div>
            </div>
            
            <div class="roster_table_container">
                <div id="roster_table_pitchers" class="roster_table"><table id="pTable" border="0" cellpadding="0" cellspacing="0"></table></div>
                <div id="roster_table_catchers" class="roster_table"><table id="cTable" border="0" cellpadding="0" cellspacing="0"></table></div>
                <div id="roster_table_infield" class="roster_table"><table id="iTable" border="0" cellpadding="0" cellspacing="0"></table></div>
                <div id="roster_table_outfield" class="roster_table"><table id="oTable" border="0" cellpadding="0" cellspacing="0"></table></div>
                <div id="roster_table_coaches" class="roster_table"><table id="coachesTable" border="0" cellpadding="0" cellspacing="0"></table></div>
            </div>
            
            <p class="legend"><span class="star">*</span> Not on Active Roster<br /><span class="star">**</span> Not on 40-Man Roster</p>
            <?php /*<div id="roster_table">
            	<table id="myTable" border="0" cellpadding="0" cellspacing="0">
                	<thead><tr><th>Jersey #</th><th>Name</th></tr></thead>
                	<tbody></tbody>
                </table>
            </div>
			*/ ?>
            
          </div>
        </div><!-- /.row -->
    </div>
    
<% } else if (viewType == "player"){  %> 
	<div class="container player-page">
        <div class="player-header" style="background-image: url('https://securea.mlb.com/images/players/action_shots/<%= id %>.jpg');"><div class="player-header-container"></div></div>
        <div class="row">
          <div class="col-md-3">
        	<div class="player-headshot"><img src="https://securea.mlb.com/mlb/images/players/head_shot/<%= id %>.jpg" /></div>
          </div>
          <div class="col-md-9">
          	<div class="player-header-text">
                <div class="player-header-main"><%= fullName %> #<%= primaryNumber %><br /></div>
                <div class="player-header-sub"><%= primaryPosition_abbreviation %> | B/T: <%= batSide_code %>/<%= pitchHand_code %> | <%= height %>/<%= weight %> | Age: <%= currentAge %></div>
            </div>
          </div>
        </div><!-- /.row -->
        
        <div class="row">
          <div class="col-lg-4">
          	<div class="detailed-info">
                <strong><%= fullFMLName %></strong><br />    
                <% if (nickName){ %>            
                	<strong>Nickname:</strong> <%= nickName %><br />
                <% } %>
                <strong>Born:</strong> <%= birthDate %> in <%= birthCity %><% if (birthStateProvince){ %>, <%= birthStateProvince %><% } %><br />
                <% if (isDrafted == 1){ %>
                    <strong>Draft:</strong> <%= draftYear %>, <%= draft_team %>, Round: <%= pickRound %>, Overall Pick: <%= pickNumber %><br />
                <% } %>
                <% if (school){ %>
                    <strong>School:</strong> <%= school %><br />
                <% } %>
                <% if (mlbDebutDate){ %>
                    <strong>Debut:</strong> <%= mlbDebutDate %><br />
                <% } %>
          	</div>
          </div>
          <div class="col-lg-8">
            <div class="stats_table_container">
                <div id="stats_table_summary" class="stats_table"><table id="sTable" border="0" cellpadding="0" cellspacing="0"></table></div>
            </div>
            <p class="player_status">-</p>
          </div>
        </div><!-- /.row -->
        
        <div class="row">
          <div class="col-md-12">
            
            <div class="tabMain">
                <div class="navWrap">
                    <ul class="clearfix">
                        <li><a href="#" id="nav_pitching" class=""><span class="line"><span>Pitching</span></span></a></li>
                        <li><a href="#" id="nav_batting" class=""><span class="line"><span>Batting</span></span></a></li>
                        <li><a href="#" id="nav_fielding" class=""><span class="line noline"><span>Fielding</span></span></a></li>
                    </ul>
                </div>
            </div>
            
            <div class="stats_table_container">
                <p class="table-headings">Career Stats</p>
                <div id="stats_table_career" class="stats_table"><table id="careerTable" border="0" cellpadding="0" cellspacing="0"></table></div>
                <p class="table-headings adv">Advanced Career Stats</p>
                <div id="stats_table_careerAdvanced" class="stats_table"><table id="careerAdvancedTable" border="0" cellpadding="0" cellspacing="0"></table></div>
                <div id="stats_table_careerAdvanced2" class="stats_table"><table id="careerAdvancedTable2" border="0" cellpadding="0" cellspacing="0"></table></div>
            </div>
            
          </div>
        </div><!-- /.row -->
    </div>
    
<% } else { %>

<% }  %>


