<?php 
/*
// uses JSON-rest-api
*/

date_default_timezone_set('America/Toronto');

class MyPlugin_API_MLB {
    public function register_routes( $routes ) {

		$routes['/baseball/teams'] = array( array( array( $this, 'get_teams'), WP_JSON_Server::READABLE ) );
		$routes['/baseball/team/(?P<team_code>\w+)'] = array( array( array( $this, 'get_team'), WP_JSON_Server::READABLE ) );
		$routes['/baseball/team/(?P<team_code>\w+)/40man'] = array( array( array( $this, 'get_team_40man'), WP_JSON_Server::READABLE ) );
		$routes['/baseball/team/(?P<team_code>\w+)/coaches'] = array( array( array( $this, 'get_team_coaches'), WP_JSON_Server::READABLE ) );
		$routes['/baseball/player/(?P<player_id>\w+)'] = array( array( array( $this, 'get_player'), WP_JSON_Server::READABLE ) );
		
		$routes['/baseball/product/new'] = array( array( array( $this, 'new_product'), WP_JSON_Server::READABLE ) );
		$routes['/baseball/product/edit/(?P<id>\w+)'] = array(array( array( $this, 'edit_product'), WP_JSON_Server::READABLE ) );
		$routes['/baseball/product/draft'] = array(array( array( $this, 'create_draft_product'), WP_JSON_Server::CREATABLE | WP_JSON_Server::EDITABLE | WP_JSON_Server::ACCEPT_JSON ) );
		$routes['/baseball/product/update'] = array(array( array( $this, 'update_product'), WP_JSON_Server::CREATABLE | WP_JSON_Server::EDITABLE | WP_JSON_Server::ACCEPT_JSON ) );
		$routes['/baseball/product/draft_to_cart'] = array(array( array( $this, 'draft_to_cart'), WP_JSON_Server::CREATABLE | WP_JSON_Server::EDITABLE | WP_JSON_Server::ACCEPT_JSON ) );
		
		$routes['/baseball/test'] = array(array( array( $this, 'test_test'), WP_JSON_Server::READABLE ) );
        return $routes;
    }
	
	public function test_test() {
		echo 'test';
		return 'okay';
	}
	
	public function get_teams() {
		$response_get = file_get_contents_curl('https://statsapi.mlb.com/api/v1/teams?sportId=1');
		$data = json_decode($response_get, true);
		
		$Aleague_array = array();
		$Nleague_array = array();
		$teams_array = array();
		foreach( $data['teams'] as $team) {
			//print_r($team);
			if ($team['league']['name'] == 'American League'){
				$Aleague_array[] = array("id" => $team['id'], "name" => $team['name'], "teamCode" => $team['teamCode'], "locationName" => $team['locationName'], "division_name" => $team['division']['name'], "division_id" => $team['division']['id']);
			} else if ($team['league']['name'] == 'National League'){
				$Nleague_array[] = array("id" => $team['id'], "name" => $team['name'], "teamCode" => $team['teamCode'], "locationName" => $team['locationName'], "division_name" => $team['division']['name'], "division_id" => $team['division']['id']);
			} 
			$teams_array[] = array("id" => $team['id'], "name" => $team['name'], "teamCode" => $team['teamCode'], "locationName" => $team['locationName'], "league" => $team['league']['name'], "division_name" => $team['division']['name'], "division_id" => $team['division']['id']);
			
		}
		
		$teams = new stdClass();
		$teams->Aleague = $Aleague_array;
		$teams->Nleague = $Nleague_array;
		$teams->teams = $teams_array;
		return $teams;
	}
	
	public function get_team($team_code) {
		$response_get = file_get_contents_curl('https://statsapi.mlb.com/api/v1/teams?sportId=1');
		$data = json_decode($response_get, true);
		
		$team_names_array = array();
		foreach( $data['teams'] as $team) {
			if ($team['teamCode'] == $team_code){
				$id = $team['id'];
				$team_name = $team['teamName'];
			}
			$team_names_array[$team['id']] = $team['abbreviation'];
		}		
		
		//$response_get = file_get_contents_curl('https://statsapi.mlb.com/api/v1/teams/'.$id.'/roster');
		$response_get = file_get_contents_curl('https://statsapi.mlb.com/api/v1/teams/'.$id.'/roster/Active?hydrate=person(stats(type=season))');
		$data = json_decode($response_get, true);
		//print_r($data);
		
		$roster_array = array();
		foreach( $data['roster'] as $player) {
			$player_obj = new stdClass();
			$player_obj->batSide_code = $player['person']['batSide']['code'];
			$player_obj->batSide_description = $player['person']['batSide']['description'];
			$player_obj->pitchHand_code = $player['person']['pitchHand']['code'];
			$player_obj->pitchHand_description = $player['person']['pitchHand']['description'];
			$player_obj->height = $player['person']['height'];
			$player_obj->weight = $player['person']['weight'];
			$player_obj->birthDate = $player['person']['birthDate'];
			
			$roster_array[] = array("id" => $player['person']['id'], "name" => $player['person']['fullName'], "jerseyNumber" => $player['jerseyNumber'], "position" => array("code" => $player['position']['code'], "name" => $player['position']['name'], "type" => $player['position']['type'], "abbreviation" => $player['position']['abbreviation']), "status_code" => $player['status']['code'], "status_description" => $player['status']['description'], "isPlayer" => 1, "player" => $player_obj);
		}
		
		$team = new stdClass();
		$team->roster = $roster_array;
		$team->team_id = $id;
		$team->team_name = $team_name;
		$team->team_names = $team_names_array;
		
		/*if ($player->currentTeam){
			
			$player->next_game = $data_next['teams'][0]['nextGameSchedule']['dates'][0]['date'];
			$player->home_team = $data_next['teams'][0]['nextGameSchedule']['dates'][0]['games'][0]['teams']['home']['team']['id'];
			$player->away_team = $data_next['teams'][0]['nextGameSchedule']['dates'][0]['games'][0]['teams']['home']['team']['id'];
		} */
		
		$response_next = file_get_contents_curl('https://statsapi.mlb.com/api/v1/teams/'.$id.'?hydrate=nextSchedule');
		$data_next = json_decode($response_next, true);
		
		/*echo '~~';
		echo $data_next['teams'][0]['nextGameSchedule']['dates'][0]['date'];
		echo $data_next['teams'][0]['nextGameSchedule']['dates'][0]['games'][0]['teams']['home']['team']['id'].':';
		echo $data_next['teams'][0]['nextGameSchedule']['dates'][0]['games'][0]['teams']['away']['team']['id'];
		//echo $data['people'][0]['rosterEntries'][0]['status']['code'];
		//echo $data['people'][0]['rosterEntries'][0]['status']['description/'];
		echo '___';
		
		print_r($data_next);*/
		$team->next_game = $data_next['teams'][0]['nextGameSchedule']['dates'][0]['date'];
		$team->next_gameDate = $data_next['teams'][0]['nextGameSchedule']['dates'][0]['date']['games'][0]['gameDate'];
		$team->home_team = $data_next['teams'][0]['nextGameSchedule']['dates'][0]['games'][0]['teams']['home']['team']['id'];
		$team->away_team = $data_next['teams'][0]['nextGameSchedule']['dates'][0]['games'][0]['teams']['away']['team']['id'];
		$team->home_wins = $data_next['teams'][0]['nextGameSchedule']['dates'][0]['games'][0]['teams']['home']['leagueRecord']['wins'];
		$team->away_wins = $data_next['teams'][0]['nextGameSchedule']['dates'][0]['games'][0]['teams']['away']['leagueRecord']['wins'];
		$team->home_losses = $data_next['teams'][0]['nextGameSchedule']['dates'][0]['games'][0]['teams']['home']['leagueRecord']['losses'];
		$team->away_losses = $data_next['teams'][0]['nextGameSchedule']['dates'][0]['games'][0]['teams']['away']['leagueRecord']['losses'];
		
		/*
		Walk LEADER -> https://statsapi.mlb.com/api/v1/teams/141/leaders?leaderCategories=walks&season=2019
		*/
		$response_leaders = file_get_contents_curl('https://statsapi.mlb.com/api/v1/teams/'.$id.'/leaders/?leaderCategories=homeRuns,runs,stolenBases,wins,saves&season=2019&limit=1');
		$data_leaders = json_decode($response_leaders, true);
		
		$team_leaders = array();
		foreach( $data_leaders['teamLeaders'] as $curLeader) {

			if ( ($curLeader['statGroup'] == 'hitting' && $curLeader['leaderCategory'] == 'homeRuns') || ($curLeader['statGroup'] == 'hitting' && $curLeader['leaderCategory'] == 'runs') || ($curLeader['statGroup'] == 'hitting' && $curLeader['leaderCategory'] == 'stolenBases') || ($curLeader['statGroup'] == 'pitching' && $curLeader['leaderCategory'] == 'wins') || ($curLeader['statGroup'] == 'pitching' && $curLeader['leaderCategory'] == 'saves') ){
		 		$team_leaders[] = array("types" => $curLeader['leaderCategory'], 'name' => $curLeader['leaders'][0]['person']['fullName'], 'player_id' => $curLeader['leaders'][0]['person']['id'], 'value' => $curLeader['leaders'][0]['value'] );
		  
			} 
		}
		$team->team_leaders = $team_leaders;
		
		return $team;
	}
	
	public function get_team_40man($team_code) {
		$response_get = file_get_contents_curl('https://statsapi.mlb.com/api/v1/teams?sportId=1');
		$data = json_decode($response_get, true);
		
		//$team_names_array = array();
		foreach( $data['teams'] as $team) {
			if ($team['teamCode'] == $team_code){
				$id = $team['id'];
				$team_name = $team['teamName'];
			}
			//$team_names_array[$team['id']] = $team['abbreviation'];
		}

		//$response_get = file_get_contents_curl('https://statsapi.mlb.com/api/v1/teams/'.$id.'/roster/40Man');
		$response_get = file_get_contents_curl('https://statsapi.mlb.com/api/v1/teams/'.$id.'/roster/40Man?hydrate=person(stats(type=season))');
		$data = json_decode($response_get, true);
		//print_r($data);
		$roster_array = array();
		foreach( $data['roster'] as $player) {
			$player_obj = new stdClass();
			$player_obj->batSide_code = $player['person']['batSide']['code'];
			$player_obj->batSide_description = $player['person']['batSide']['description'];
			$player_obj->pitchHand_code = $player['person']['pitchHand']['code'];
			$player_obj->pitchHand_description = $player['person']['pitchHand']['description'];
			$player_obj->height = $player['person']['height'];
			$player_obj->weight = $player['person']['weight'];
			$player_obj->birthDate = $player['person']['birthDate'];
			
			$roster_array[] = array("id" => $player['person']['id'], "name" => $player['person']['fullName'], "jerseyNumber" => $player['jerseyNumber'], "position" => array("code" => $player['position']['code'], "name" => $player['position']['name'], "type" => $player['position']['type'], "abbreviation" => $player['position']['abbreviation']), "status_code" => $player['status']['code'], "status_description" => $player['status']['description'], "isPlayer" => 1, "player" => $player_obj);
		}
		
		$team = new stdClass();
		$team->roster = $roster_array;
		$team->team_id = $id;
		$team->team_name = $team_name;
		return $team;
	}
	
	public function get_team_coaches($team_code) {
		$response_get = file_get_contents_curl('https://statsapi.mlb.com/api/v1/teams?sportId=1');
		$data = json_decode($response_get, true);
		
		//$team_names_array = array();
		foreach( $data['teams'] as $team) {
			if ($team['teamCode'] == $team_code){
				$id = $team['id'];
				$team_name = $team['teamName'];
			}
			//$team_names_array[$team['id']] = $team['abbreviation'];
		}

		$response_get = file_get_contents_curl('https://statsapi.mlb.com/api/v1/teams/'.$id.'/coaches');
		$data = json_decode($response_get, true);
		$roster_array = array();
		foreach( $data['roster'] as $coach) {
			$coach_obj = new stdClass();
			
			$roster_array[] = array("id" => $coach['person']['id'], "name" => $coach['person']['fullName'], "jerseyNumber" => $coach['jerseyNumber'], "job" => $coach['job'], "isPlayer" => 0);
		}
		
		$team = new stdClass();
		$team->roster = $roster_array;
		$team->team_id = $id;
		$team->team_name = $team_name;
		return $team;
	}
		
	public function get_player($player_id) {
		$response_names = file_get_contents_curl('https://statsapi.mlb.com/api/v1/teams?sportId=1');
		$data_name = json_decode($response_names, true);
		
		$team_names_array = array();
		foreach( $data_name['teams'] as $team) {
			$team_names_array[$team['id']] = $team['abbreviation'];
		}
		
		//https://statsapi.mlb.com//api/v1/people/475253?hydrate=stats(group=[hitting,pitching,fielding],type=[yearByYear])
		$response_get = file_get_contents_curl('https://statsapi.mlb.com/api/v1/people/'.$player_id.'?hydrate=currentTeam,rosterEntries,stats(group=[hitting,pitching,fielding],type=[yearByYear,career])');
		$data = json_decode($response_get, true);		
		
		$response_draft = file_get_contents_curl('https://statsapi.mlb.com/api/v1/draft/'.$data['people'][0]['draftYear'].'?playerId='.$player_id);
		$data_draft = json_decode($response_draft, true);
				
		$response_advanced = file_get_contents_curl('https://statsapi.mlb.com/api/v1/people/'.$player_id.'?hydrate=stats(group=[hitting,pitching,fielding],type=[yearByYearAdvanced,careerAdvanced])');
		$data_advanced = json_decode($response_advanced, true);
				
		//print_r($data);
				
		/*echo $data['people'][0]['id'];
		echo '___';
		//print_r($data['people'][0]['stats']);
		print_r($data['people'][0]['stats'][0]['splits']);
		//*/
		
		$player = new stdClass();
		$player->id = $data['people'][0]['id'];
		$player->fullName = $data['people'][0]['fullName'];
		$player->fullFMLName = $data['people'][0]['fullFMLName'];
		$player->firstName = $data['people'][0]['firstName'];
		$player->lastName = $data['people'][0]['lastName'];
		$player->nickName = $data['people'][0]['nickName'];
		$player->primaryNumber = $data['people'][0]['primaryNumber'];
		$player->birthDate = $data['people'][0]['birthDate'];
		$player->currentAge = $data['people'][0]['currentAge'];
		$player->birthCity = $data['people'][0]['birthCity'];
		$player->birthStateProvince = $data['people'][0]['birthStateProvince'];
		$player->birthCountry = $data['people'][0]['birthCountry'];
		$player->height = $data['people'][0]['height'];
		$player->weight = $data['people'][0]['weight'];
		$player->active = $data['people'][0]['active'];
		//$player->primaryPosition = array("code" => $data['people'][0]['primaryPosition']['code'], "name" => $data['people'][0]['primaryPosition']['name'], "type" => $data['people'][0]['primaryPosition']['type'], "abbreviation" => $data['people'][0]['primaryPosition']['abbreviation']);
		$player->primaryPosition_code = $data['people'][0]['primaryPosition']['code'];
		$player->primaryPosition_name = $data['people'][0]['primaryPosition']['name'];
		$player->primaryPosition_type = $data['people'][0]['primaryPosition']['type'];
		$player->primaryPosition_abbreviation = $data['people'][0]['primaryPosition']['abbreviation'];
		$player->batSide_code = $data['people'][0]['batSide']['code'];
		$player->batSide_description = $data['people'][0]['batSide']['description'];
		$player->pitchHand_code = $data['people'][0]['pitchHand']['code'];
		$player->pitchHand_description = $data['people'][0]['pitchHand']['description'];
		
		$player->currentTeam = $data['people'][0]['currentTeam']['id'];
		$player->currentTeam_name = $team_names_array[$data['people'][0]['currentTeam']['id']];
		$player->status_code = '';
		$player->status_description = '';
		if ($player->currentTeam){
			//$player->status_code = $data['people'][0]['rosterEntries'][0]['status']['code'];
			
			$response_next = file_get_contents_curl('https://statsapi.mlb.com/api/v1/teams/'.$player->currentTeam.'?hydrate=nextSchedule');
			$data_next = json_decode($response_next, true);
			/*
			echo '~~';
			echo $data_next['teams'][0]['nextGameSchedule']['dates'][0]['date'];
			echo $data_next['teams'][0]['nextGameSchedule']['dates'][0]['games'][0]['teams']['home']['team']['id'].':';
			echo $data_next['teams'][0]['nextGameSchedule']['dates'][0]['games'][0]['teams']['away']['team']['id'];
			//echo $data['people'][0]['rosterEntries'][0]['status']['code'];
			//echo $data['people'][0]['rosterEntries'][0]['status']['description/'];
			echo '___';
			
			print_r($data_next);*/
			$player->next_game = $data_next['teams'][0]['nextGameSchedule']['dates'][0]['date'];
			$player->home_team = $data_next['teams'][0]['nextGameSchedule']['dates'][0]['games'][0]['teams']['home']['team']['id'];
			$player->away_team = $data_next['teams'][0]['nextGameSchedule']['dates'][0]['games'][0]['teams']['home']['team']['id'];
		} 
		if ($data['people'][0]['rosterEntries'][0]['status']['code']){
			$player->status_code = $data['people'][0]['rosterEntries'][0]['status']['code'];
		} 
		if ($data['people'][0]['rosterEntries'][0]['status']['description']){
			$player->status_description = $data['people'][0]['rosterEntries'][0]['status']['description'];
		} 
				
		$player->draftYear = $data['people'][0]['draftYear'];
		$player->mlbDebutDate = $data['people'][0]['mlbDebutDate'];
		
		$player->pickRound = $data_draft['drafts']['rounds'][0]['picks'][0]['pickRound'];
		$player->pickNumber = $data_draft['drafts']['rounds'][0]['picks'][0]['pickNumber'];
		$player->school = $data_draft['drafts']['rounds'][0]['picks'][0]['school']['name'];
		$player->draft_team = $data_draft['drafts']['rounds'][0]['picks'][0]['team']['name'];
		$player->isDrafted = $data_draft['drafts']['rounds'][0]['picks'][0]['isDrafted'];
		
		$player->team_names = $team_names_array;
		if ($data['people'][0]['primaryPosition']['type'] == 'Pitcher'){
			$player->isPitcher = 1;
		} else {
			$player->isPitcher = 0;
		}

		for ($i = 0; $i < 6; $i++){
			$statsGroup = $data['people'][0]['stats'][$i];
			if ($statsGroup['group']['displayName']) {
				if ($statsGroup['group']['displayName'] == 'hitting'){
					if ($statsGroup['type']['displayName'] == 'yearByYear'){
						$player->hitting_stats_year = $statsGroup['splits'];
					} else if ($statsGroup['type']['displayName'] == 'career'){
						$player->hitting_stats_career = $statsGroup['splits'];
					}
				} else if ($statsGroup['group']['displayName'] == 'fielding'){
					if ($statsGroup['type']['displayName'] == 'yearByYear'){
						$player->fielding_stats_year = $statsGroup['splits'];
					} else if ($statsGroup['type']['displayName'] == 'career'){
						$player->fielding_stats_career = $statsGroup['splits'];
					}
				} else if ($statsGroup['group']['displayName'] == 'pitching'){
					if ($statsGroup['type']['displayName'] == 'yearByYear'){
						$player->pitching_stats_year = $statsGroup['splits'];
					} else if ($statsGroup['type']['displayName'] == 'career'){
						$player->pitching_stats_career = $statsGroup['splits'];
					}
				}
			} 
			
			$statsGroupAdv = $data_advanced['people'][0]['stats'][$i];
			if ($statsGroupAdv['group']['displayName']) {
				if ($statsGroupAdv['group']['displayName'] == 'hitting'){
					if ($statsGroupAdv['type']['displayName'] == 'yearByYearAdvanced'){
						$player->hitting_stats_yearAdv = $statsGroupAdv['splits'];
					} else if ($statsGroupAdv['type']['displayName'] == 'careerAdvanced'){
						$player->hitting_stats_careerAdv = $statsGroupAdv['splits'];
					}
				} else if ($statsGroupAdv['group']['displayName'] == 'pitching'){
					if ($statsGroupAdv['type']['displayName'] == 'yearByYearAdvanced'){
						$player->pitching_stats_yearAdv = $statsGroupAdv['splits'];
					} else if ($statsGroupAdv['type']['displayName'] == 'careerAdvanced'){
						$player->pitching_stats_careerAdv = $statsGroupAdv['splits'];
					}
				}
			} 
		}
		
		return $player;
	}
		
}

	function file_get_contents_curl($url) {
		/*$ch = curl_init();
	
		curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 6.1; rv:19.0) Gecko/20100101 Firefox/19.0');
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_URL, $url);
	
		$data = curl_exec($ch);
		curl_close($ch);*/
		
		$curl = curl_init();

		curl_setopt_array($curl, array(
		  CURLOPT_URL => $url,
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_TIMEOUT => 60,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "GET",
		  CURLOPT_HTTPHEADER => array(
			"cache-control: no-cache"
		  ),
		));
		curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		
		$data = curl_exec($curl);
		$err = curl_error($curl);
		if ($err){ print_r($err); }
	
		return $data;
	}

	function get_baseball_connection() {
		$wpdb_products = new wpdb(DB_USER, DB_PASSWORD, 'kmi_products', DB_HOST);
		global $cur_db_prefix;
		$cur_db_prefix = 'cc';
		return $wpdb_products;
	}

	function get_site_connection() {
		//$wpdb_cs = new wpdb(DB_USER, DB_PASSWORD, 'cs_constructionspecifier_com', DB_HOST);
		$wpdb_site = new wpdb(DB_USER, DB_PASSWORD, 'wordpress', DB_HOST);
		global $cur_db_prefix;
		$cur_db_prefix = 'cc';
		return $wpdb_site;
	}	
	

