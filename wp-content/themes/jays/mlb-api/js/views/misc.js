jQuery(function ($) {
	window.MiscView = Backbone.View.extend({
		initialize:function (options) {
			//this.render();
      		this.options = options;
			this.render();
		},
	
		events:{
			"click #nav_active":"activeClicked",
			"click #nav_40":"fortyClicked",
			"click #nav_coaches":"coachesClicked",
			
			"click #nav_pitching":"pitchingClicked",
			"click #nav_batting":"battingClicked",
			"click #nav_fielding":"fieldingClicked"
		},
			
		render:function () {
			//$(this.el).html(this.template({viewType: this.options.viewType}));
			if (this.options.viewType == "home"){
				$(this.el).html(this.template({viewType: this.options.viewType}));	
				
			} else if (this.options.viewType == "teams"){
				$(this.el).html(this.template({viewType: this.options.viewType}));	
				
				var teams = this.model.get('teams');
				var innerTable_A = '';
				var innerTable_N = '';
				
				for (var i = 0; i < teams.length; i++) {
					var team_link = '#team/' + teams[i]['teamCode'];
					//var card_class = "team_card animated fadeInUp";
					var card_class = "team_card animated";
					if (teams[i]['league'] == 'American League'){
						innerTable_A += '<div class="animated fadeInUp"><a href="'+team_link+'"><div class="team_card"><img src="https://www.mlbstatic.com/team-logos/'+ teams[i]['id']+'.svg" /><p>'+teams[i]['name']+'</p></div></a></div>';
					} else if (teams[i]['league'] == 'National League'){
						innerTable_N += '<div class="animated fadeInUp"><a href="'+team_link+'"><div class="team_card"><img src="https://www.mlbstatic.com/team-logos/'+ teams[i]['id']+'.svg" /><p>'+teams[i]['name']+'</p></div></a></div>';
					}
				}
				this.$("#aleague_list").html(innerTable_A);
				this.$("#nleague_list").html(innerTable_N);
				
			} else if (this.options.viewType == "teams_al"){
				$(this.el).html(this.template({viewType: this.options.viewType}));	
				
				var teams = this.model.get('Aleague');
				var innerTable_div1 = '';
				var innerTable_div2 = '';
				var innerTable_div3 = '';
				for (var i = 0; i < teams.length; i++) {
					var team_link = '#team/' + teams[i]['teamCode'];
					//var card_class = "team_card animated fadeInUp";
					if (teams[i]['division_name'] == 'American League East'){
						innerTable_div1 += '<div class="animated fadeInUp"><a href="'+team_link+'"><div class="team_card"><img src="https://www.mlbstatic.com/team-logos/'+ teams[i]['id']+'.svg" /><p>'+teams[i]['name']+'</p></div></a></div>';
					} else if (teams[i]['division_name'] == 'American League Central'){
						innerTable_div2 += '<div class="animated fadeInUp"><a href="'+team_link+'"><div class="team_card"><img src="https://www.mlbstatic.com/team-logos/'+ teams[i]['id']+'.svg" /><p>'+teams[i]['name']+'</p></div></a></div>';
						
					} else if (teams[i]['division_name'] == 'American League West'){
						innerTable_div3 += '<div class="animated fadeInUp"><a href="'+team_link+'"><div class="team_card"><img src="https://www.mlbstatic.com/team-logos/'+ teams[i]['id']+'.svg" /><p>'+teams[i]['name']+'</p></div></a></div>';
					}
				}
				this.$("#division1").html(innerTable_div1);
				this.$("#division2").html(innerTable_div2);
				this.$("#division3").html(innerTable_div3);
				
			} else if (this.options.viewType == "teams_nl"){
				$(this.el).html(this.template({viewType: this.options.viewType}));	
				
				var teams = this.model.get('Nleague');
				var innerTable_div1 = '';
				var innerTable_div2 = '';
				var innerTable_div3 = '';
				for (var i = 0; i < teams.length; i++) {
					var team_link = '#team/' + teams[i]['teamCode'];
					var card_class = "team_card animated fadeInUp";
					if (teams[i]['division_name'] == 'National League East'){
						innerTable_div1 += '<div class="animated fadeInUp"><a href="'+team_link+'"><div class="team_card"><img src="https://www.mlbstatic.com/team-logos/'+ teams[i]['id'] +'.svg" /><p>' + teams[i]['name'] + '</p></div></a></div>';
					} else if (teams[i]['division_name'] == 'National League Central'){
						innerTable_div2 += '<div class="animated fadeInUp"><a href="'+team_link+'"><div class="team_card"><img src="https://www.mlbstatic.com/team-logos/'+ teams[i]['id'] +'.svg" /><p>' + teams[i]['name'] + '</p></div></a></div>';
						
					} else if (teams[i]['division_name'] == 'National League West'){
						innerTable_div3 += '<div class="animated fadeInUp"><a href="'+team_link+'"><div class="team_card"><img src="https://www.mlbstatic.com/team-logos/'+ teams[i]['id'] +'.svg" /><p>' + teams[i]['name'] + '</p></div></a></div>';
					}
				}
				this.$("#division1").html(innerTable_div1);
				this.$("#division2").html(innerTable_div2);
				this.$("#division3").html(innerTable_div3);
				
			} else if (this.options.viewType == "team"){
				var MLB_teams = this.model.get('team_names');
				$(this.el).html(this.template({viewType: this.options.viewType}));	
				
				var away_link = '#team/' + MLB_teams[this.model.get('away_team')];
				var away_col = '<a href="'+away_link+'"><img class="away_logo next_logo" src="https://www.mlbstatic.com/team-logos/'+this.model.get('away_team')+'.svg"></a>';
				away_col += '<div class="away_team"><a href="'+away_link+'">'+MLB_teams[this.model.get('away_team')]+'</a></div>';
				away_col += '<div class="away_record">'+this.model.get('away_wins')+'-'+this.model.get('away_losses')+'</div>';
				this.$("#nextgame_matchup .col1").html(away_col);
				
				var home_link = '#team/' + MLB_teams[this.model.get('home_team')];
				var home_col = '<a href="'+home_link+'"><img class="home_logo next_logo" src="https://www.mlbstatic.com/team-logos/'+this.model.get('home_team')+'.svg"></a>';
				home_col += '<div class="home_team"><a href="'+home_link+'">'+MLB_teams[this.model.get('home_team')]+'</a></div>';
				home_col += '<div class="home_record">'+this.model.get('home_wins')+'-'+this.model.get('home_losses')+'</div>';
				this.$("#nextgame_matchup .col3").html(home_col);
			
				var team_roster = this.model.get('roster');
				this.refreshTable(this.model.toJSON());
								
				this.$(".page-header").html(this.model.get('team_name') + " Roster & Staff");
				
				var team_leaders = this.model.get('team_leaders');
				var leaderHTML = '';
				var catName = '';				
				for (var i = 0; i < team_leaders.length; i++) {          
				  if (team_leaders[i]['types'] == 'homeRuns'){
					catName = 'Home Runs';
				  } else if (team_leaders[i]['types'] == 'runs'){
					catName = 'Runs';
				  } else if (team_leaders[i]['types'] == 'stolenBases'){
					catName = 'Stolen Bases';
				  } else if (team_leaders[i]['types'] == 'wins'){
					catName = 'Wins';
				  } else if (team_leaders[i]['types'] == 'saves'){
					catName = 'Saves';
				  }
				  leaderHTML += '<strong>'+catName + ':</strong> ' + team_leaders[i]['name'] +'('+team_leaders[i]['value']+')<br />';
				}
				
				this.$("#team_leaders").html(leaderHTML);
				
			} else if (this.options.viewType == "player"){	
			
				this.model.set('viewType', this.options.viewType);
				$(this.el).html(this.template(this.model.toJSON()));
				
				if (this.model.get('isPitcher') == 0){
					this.$(".tabMain").addClass('sm');
					this.$("#nav_batting").addClass('tab_current');
					this.$("#nav_pitching").hide();
				} else {
					this.$("#nav_pitching").addClass('tab_current');
				}
				
				this.model.set('table_mode', 'default');
				this.renderTable();
								
			} 
			$('body').scrollspy('refresh'); 
			return this;
		},
		
		renderTable:function () {
			var MLB_teams = this.model.get('team_names');
			
      		var currentStats_html = '';
			if (this.model.get('primaryPosition_name') == 'Pitcher'){
				var current_stats = this.model.get('pitching_stats_year')[this.model.get('pitching_stats_year').length-1];	
				var career_stats = this.model.get('pitching_stats_career')[0];				
				var tableHeadings = '<thead><tr><th>Year</th><th>W</th><th>L</th><th>ERA</th><th>G</th><th>GS</th><th>SV</th><th>IP</th><th>SO</th><th>WHIP</th></tr></thead>';
				
				currentStats_html += '<tr><td>' + current_stats['season'] + ' Stats</td>';
				currentStats_html += '<td>' + current_stats['stat']['wins'] + '</td>';
				currentStats_html += '<td>' + current_stats['stat']['losses'] + '</td>';
				currentStats_html += '<td>' + current_stats['stat']['era'] + '</td>';
				currentStats_html += '<td>' + current_stats['stat']['gamesPitched'] + '</td>';
				currentStats_html += '<td>' + current_stats['stat']['gamesStarted'] + '</td>';
				currentStats_html += '<td>' + current_stats['stat']['saves'] + '</td>';
				currentStats_html += '<td>' + current_stats['stat']['inningsPitched'] + '</td>';
				currentStats_html += '<td>' + current_stats['stat']['strikeOuts'] + '</td>';
				currentStats_html += '<td>' + current_stats['stat']['whip'] + '</td>';
				currentStats_html += '</tr>';
				
				currentStats_html += '<tr><td>MLB Career Stats</td>';
				currentStats_html += '<td>' + career_stats['stat']['wins'] + '</td>';
				currentStats_html += '<td>' + career_stats['stat']['losses'] + '</td>';
				currentStats_html += '<td>' + career_stats['stat']['era'] + '</td>';
				currentStats_html += '<td>' + career_stats['stat']['gamesPitched'] + '</td>';
				currentStats_html += '<td>' + career_stats['stat']['gamesStarted'] + '</td>';
				currentStats_html += '<td>' + career_stats['stat']['saves'] + '</td>';
				currentStats_html += '<td>' + career_stats['stat']['inningsPitched'] + '</td>';
				currentStats_html += '<td>' + career_stats['stat']['strikeOuts'] + '</td>';
				currentStats_html += '<td>' + career_stats['stat']['whip'] + '</td>';
				currentStats_html += '</tr>';
				
				var chart_type = 'pitching';
			} else{	
				var current_stats = this.model.get('hitting_stats_year')[this.model.get('hitting_stats_year').length-1];
				var career_stats = this.model.get('hitting_stats_career')[0];				
				var tableHeadings = '<thead><tr><th>Year</th><th>AB</th><th>R</th><th>H</th><th>HR</th><th>RBI</th><th>SB</th><th>AVG</th><th>OBP</th><th>OPS</th></tr></thead>';
			
				currentStats_html += '<tr><td>' + current_stats['season'] + ' Stats</td>';
				currentStats_html += '<td>' + current_stats['stat']['atBats'] + '</td>';
				currentStats_html += '<td>' + current_stats['stat']['runs'] + '</td>';
				currentStats_html += '<td>' + current_stats['stat']['hits'] + '</td>';
				currentStats_html += '<td>' + current_stats['stat']['homeRuns'] + '</td>';
				currentStats_html += '<td>' + current_stats['stat']['rbi'] + '</td>';
				currentStats_html += '<td>' + current_stats['stat']['stolenBases'] + '</td>';
				currentStats_html += '<td>' + current_stats['stat']['avg'] + '</td>';
				currentStats_html += '<td>' + current_stats['stat']['obp'] + '</td>';
				currentStats_html += '<td>' + current_stats['stat']['ops'] + '</td>';
				currentStats_html += '</tr>';
				
				currentStats_html += '<tr><td>MLB Career Stats</td>';
				currentStats_html += '<td>' + career_stats['stat']['atBats'] + '</td>';
				currentStats_html += '<td>' + career_stats['stat']['runs'] + '</td>';
				currentStats_html += '<td>' + career_stats['stat']['hits'] + '</td>';
				currentStats_html += '<td>' + career_stats['stat']['homeRuns'] + '</td>';
				currentStats_html += '<td>' + career_stats['stat']['rbi'] + '</td>';
				currentStats_html += '<td>' + career_stats['stat']['stolenBases'] + '</td>';
				currentStats_html += '<td>' + career_stats['stat']['avg'] + '</td>';
				currentStats_html += '<td>' + career_stats['stat']['obp'] + '</td>';
				currentStats_html += '<td>' + career_stats['stat']['ops'] + '</td>';
				currentStats_html += '</tr>';
				
				var chart_type = 'batting';
			}
			this.$("#sTable").html(tableHeadings+'<tbody>'+currentStats_html+'</tbody>');
			if (this.model.get('currentTeam') == this.model.get('home_team')){
				var home_away = 'vs '+MLB_teams[this.model.get('away_team')];
			} else {		
				var home_away = '@ '+MLB_teams[this.model.get('home_team')];
			}
			this.$(".player_status").html('<strong>Status:</strong> '+this.model.get('status_description')+' | <strong>Next '+this.model.get('currentTeam_name')+' Game:</strong> ' + this.model.get('next_game') +' ' + home_away);	
			
			if (this.model.get('table_mode') != 'default'){
				chart_type = this.model.get('table_mode');
			}
			
			var seasonStats_html = '';
			if (chart_type == 'pitching'){
				var season_stats = this.model.get('pitching_stats_year');
				var career_stats = this.model.get('pitching_stats_career')[0];	
				for (var i = 0; i < season_stats.length; i++) {
					if (season_stats[i]['numTeams']){	// Do not display
					} else {
																 
						seasonStats_html += '<tr><td>' + season_stats[i]['season'] + '</td>';
						seasonStats_html += '<td>' + MLB_teams[season_stats[i]['team']['id']] + '</td>';
						if (season_stats[i]['league']['name'] == 'American League'){
							seasonStats_html += '<td>AL</td>';
						} else if (season_stats[i]['league']['name'] == 'National League'){
							seasonStats_html += '<td>NL</td>';
						}
						seasonStats_html += '<td>' + season_stats[i]['stat']['wins'] + '</td>';
						seasonStats_html += '<td>' + season_stats[i]['stat']['losses'] + '</td>';
						seasonStats_html += '<td>' + season_stats[i]['stat']['era'] + '</td>';
						seasonStats_html += '<td>' + season_stats[i]['stat']['gamesPlayed'] + '</td>';
						seasonStats_html += '<td>' + season_stats[i]['stat']['gamesStarted'] + '</td>';
						seasonStats_html += '<td>' + season_stats[i]['stat']['completeGames'] + '</td>';
						seasonStats_html += '<td>' + season_stats[i]['stat']['shutouts'] + '</td>';
						seasonStats_html += '<td>' + season_stats[i]['stat']['holds'] + '</td>';
						seasonStats_html += '<td>' + season_stats[i]['stat']['saves'] + '</td>';
						seasonStats_html += '<td>' + season_stats[i]['stat']['saveOpportunities'] + '</td>';
						seasonStats_html += '<td>' + season_stats[i]['stat']['inningsPitched'] + '</td>';
						seasonStats_html += '<td>' + season_stats[i]['stat']['hits'] + '</td>';
						seasonStats_html += '<td>' + season_stats[i]['stat']['runs'] + '</td>';
						seasonStats_html += '<td>' + season_stats[i]['stat']['earnedRuns'] + '</td>';
						seasonStats_html += '<td>' + season_stats[i]['stat']['homeRuns'] + '</td>';
						seasonStats_html += '<td>' + season_stats[i]['stat']['numberOfPitches'] + '</td>';
						seasonStats_html += '<td>' + season_stats[i]['stat']['hitBatsmen'] + '</td>';
						seasonStats_html += '<td>' + season_stats[i]['stat']['baseOnBalls'] + '</td>';
						seasonStats_html += '<td>' + season_stats[i]['stat']['intentionalWalks'] + '</td>';
						seasonStats_html += '<td>' + season_stats[i]['stat']['strikeOuts'] + '</td>';
						seasonStats_html += '<td>' + season_stats[i]['stat']['avg'] + '</td>';
						seasonStats_html += '<td>' + season_stats[i]['stat']['whip'] + '</td>';
						seasonStats_html += '<td>' + season_stats[i]['stat']['groundOutsToAirouts'] + '</td>';
						seasonStats_html += '</tr>';
					}
				}							 
				seasonStats_html += '<tr><td>MLB Career</td>';
				seasonStats_html += '<td>-</td>';
				seasonStats_html += '<td>-</td>';
				seasonStats_html += '<td>' + career_stats['stat']['wins'] + '</td>';
				seasonStats_html += '<td>' + career_stats['stat']['losses'] + '</td>';
				seasonStats_html += '<td>' + career_stats['stat']['era'] + '</td>';
				seasonStats_html += '<td>' + career_stats['stat']['gamesPlayed'] + '</td>';
				seasonStats_html += '<td>' + career_stats['stat']['gamesStarted'] + '</td>';
				seasonStats_html += '<td>' + career_stats['stat']['completeGames'] + '</td>';
				seasonStats_html += '<td>' + career_stats['stat']['shutouts'] + '</td>';
				seasonStats_html += '<td>' + career_stats['stat']['holds'] + '</td>';
				seasonStats_html += '<td>' + career_stats['stat']['saves'] + '</td>';
				seasonStats_html += '<td>' + career_stats['stat']['saveOpportunities'] + '</td>';
				seasonStats_html += '<td>' + career_stats['stat']['inningsPitched'] + '</td>';
				seasonStats_html += '<td>' + career_stats['stat']['hits'] + '</td>';
				seasonStats_html += '<td>' + career_stats['stat']['runs'] + '</td>';
				seasonStats_html += '<td>' + career_stats['stat']['earnedRuns'] + '</td>';
				seasonStats_html += '<td>' + career_stats['stat']['homeRuns'] + '</td>';
				seasonStats_html += '<td>' + career_stats['stat']['numberOfPitches'] + '</td>';
				seasonStats_html += '<td>' + career_stats['stat']['hitBatsmen'] + '</td>';
				seasonStats_html += '<td>' + career_stats['stat']['baseOnBalls'] + '</td>';
				seasonStats_html += '<td>' + career_stats['stat']['intentionalWalks'] + '</td>';
				seasonStats_html += '<td>' + career_stats['stat']['strikeOuts'] + '</td>';
				seasonStats_html += '<td>' + career_stats['stat']['avg'] + '</td>';
				seasonStats_html += '<td>' + career_stats['stat']['whip'] + '</td>';
				seasonStats_html += '<td>' + career_stats['stat']['groundOutsToAirouts'] + '</td>';
				seasonStats_html += '</tr>';
									
				var tableHeadings = '<thead><tr><th>Season</th><th>Team</th><th>LG</th><th>W</th><th>L</th><th>ERA</th><th>G</th><th>GS</th><th>CG</th><th>SHO</th><th>HLD</th><th>SV</th><th>SVO</th><th>IP</th><th>H</th><th>R</th><th>ER</th><th>HR</th><th>NP</th><th>HB</th><th>BB</th><th>IBB</th><th>SO</th><th>AVG</th><th>WHIP</th><th>GO/AO</th></tr></thead>';
				
			} else if (chart_type == 'batting'){
				var season_stats = this.model.get('hitting_stats_year');
				var career_stats = this.model.get('hitting_stats_career')[0];	
				for (var i = 0; i < season_stats.length; i++) {
					if (season_stats[i]['numTeams']){	// Do not display
					} else {
																 
						seasonStats_html += '<tr><td>' + season_stats[i]['season'] + '</td>';
						seasonStats_html += '<td>' + MLB_teams[season_stats[i]['team']['id']] + '</td>';
						if (season_stats[i]['league']['name'] == 'American League'){
							seasonStats_html += '<td>AL</td>';
						} else if (season_stats[i]['league']['name'] == 'National League'){
							seasonStats_html += '<td>NL</td>';
						}
						seasonStats_html += '<td>' + season_stats[i]['stat']['gamesPlayed'] + '</td>';
						seasonStats_html += '<td>' + season_stats[i]['stat']['atBats'] + '</td>';
						seasonStats_html += '<td>' + season_stats[i]['stat']['runs'] + '</td>';
						seasonStats_html += '<td>' + season_stats[i]['stat']['hits'] + '</td>';
						seasonStats_html += '<td>' + season_stats[i]['stat']['totalBases'] + '</td>';
						seasonStats_html += '<td>' + season_stats[i]['stat']['doubles'] + '</td>';
						seasonStats_html += '<td>' + season_stats[i]['stat']['triples'] + '</td>';
						seasonStats_html += '<td>' + season_stats[i]['stat']['homeRuns'] + '</td>';
						seasonStats_html += '<td>' + season_stats[i]['stat']['rbi'] + '</td>';
						seasonStats_html += '<td>' + season_stats[i]['stat']['baseOnBalls'] + '</td>';
						seasonStats_html += '<td>' + season_stats[i]['stat']['intentionalWalks'] + '</td>';
						seasonStats_html += '<td>' + season_stats[i]['stat']['strikeOuts'] + '</td>';
						seasonStats_html += '<td>' + season_stats[i]['stat']['stolenBases'] + '</td>';
						seasonStats_html += '<td>' + season_stats[i]['stat']['caughtStealing'] + '</td>';
						seasonStats_html += '<td>' + season_stats[i]['stat']['avg'] + '</td>';
						seasonStats_html += '<td>' + season_stats[i]['stat']['obp'] + '</td>';
						seasonStats_html += '<td>' + season_stats[i]['stat']['slg'] + '</td>';
						seasonStats_html += '<td>' + season_stats[i]['stat']['ops'] + '</td>';
						seasonStats_html += '<td>' + season_stats[i]['stat']['groundOutsToAirouts'] + '</td>';
						seasonStats_html += '</tr>';
					}
				}															 
				seasonStats_html += '<tr><td>MLB Career</td>';
				seasonStats_html += '<td>-</td>';
				seasonStats_html += '<td>-</td>';
				seasonStats_html += '<td>' + career_stats['stat']['gamesPlayed'] + '</td>';
				seasonStats_html += '<td>' + career_stats['stat']['atBats'] + '</td>';
				seasonStats_html += '<td>' + career_stats['stat']['runs'] + '</td>';
				seasonStats_html += '<td>' + career_stats['stat']['hits'] + '</td>';
				seasonStats_html += '<td>' + career_stats['stat']['totalBases'] + '</td>';
				seasonStats_html += '<td>' + career_stats['stat']['doubles'] + '</td>';
				seasonStats_html += '<td>' + career_stats['stat']['triples'] + '</td>';
				seasonStats_html += '<td>' + career_stats['stat']['homeRuns'] + '</td>';
				seasonStats_html += '<td>' + career_stats['stat']['rbi'] + '</td>';
				seasonStats_html += '<td>' + career_stats['stat']['baseOnBalls'] + '</td>';
				seasonStats_html += '<td>' + career_stats['stat']['intentionalWalks'] + '</td>';
				seasonStats_html += '<td>' + career_stats['stat']['strikeOuts'] + '</td>';
				seasonStats_html += '<td>' + career_stats['stat']['stolenBases'] + '</td>';
				seasonStats_html += '<td>' + career_stats['stat']['caughtStealing'] + '</td>';
				seasonStats_html += '<td>' + career_stats['stat']['avg'] + '</td>';
				seasonStats_html += '<td>' + career_stats['stat']['obp'] + '</td>';
				seasonStats_html += '<td>' + career_stats['stat']['slg'] + '</td>';
				seasonStats_html += '<td>' + career_stats['stat']['ops'] + '</td>';
				seasonStats_html += '<td>' + career_stats['stat']['groundOutsToAirouts'] + '</td>';
				seasonStats_html += '</tr>';
				
				var tableHeadings = '<thead><tr><th>Season</th><th>Team</th><th>LG</th><th>G</th><th>AB</th><th>R</th><th>H</th><th>TB</th><th>2B</th><th>3B</th><th>HR</th><th>RBI</th><th>BB</th><th>IBB</th><th>SO</th><th>SB</th><th>CS</th><th>AVG</th><th>OBP</th><th>SLG</th><th>OPS</th><th>GO/AO</th></tr></thead>';
			
			} else if (chart_type = 'fielding'){
				var season_stats = this.model.get('fielding_stats_year');
				var career_stats = this.model.get('fielding_stats_career')[0];	
				for (var i = 0; i < season_stats.length; i++) {
					if (season_stats[i]['numTeams']){	// Do not display
					} else {
																 
						seasonStats_html += '<tr><td>' + season_stats[i]['season'] + '</td>';
						seasonStats_html += '<td>' + MLB_teams[season_stats[i]['team']['id']] + '</td>';
						if (season_stats[i]['league']['name'] == 'American League'){
							seasonStats_html += '<td>AL</td>';
						} else if (season_stats[i]['league']['name'] == 'National League'){
							seasonStats_html += '<td>NL</td>';
						}
						seasonStats_html += '<td>' + season_stats[i]['stat']['position']['abbreviation'] + '</td>';
						seasonStats_html += '<td>' + season_stats[i]['stat']['games'] + '</td>';
						seasonStats_html += '<td>' + season_stats[i]['stat']['gamesStarted'] + '</td>';
						seasonStats_html += '<td>' + season_stats[i]['stat']['innings'] + '</td>';
						seasonStats_html += '<td>' + season_stats[i]['stat']['chances'] + '</td>';
						seasonStats_html += '<td>' + season_stats[i]['stat']['putOuts'] + '</td>';
						seasonStats_html += '<td>' + season_stats[i]['stat']['assists'] + '</td>';
						seasonStats_html += '<td>' + season_stats[i]['stat']['errors'] + '</td>';
						seasonStats_html += '<td>' + season_stats[i]['stat']['doublePlays'] + '</td>';
						
						if (season_stats[i]['stat']['passedBall']) { seasonStats_html += '<td>' + season_stats[i]['stat']['passedBall'] + '</td>'; } else { seasonStats_html += '<td>-</td>'; }
						if (season_stats[i]['stat']['stolenBases']) { seasonStats_html += '<td>' + season_stats[i]['stat']['stolenBases'] + '</td>'; } else { seasonStats_html += '<td>-</td>'; }
						if (season_stats[i]['stat']['caughtStealing']) { seasonStats_html += '<td>' + season_stats[i]['stat']['caughtStealing'] + '</td>'; } else { seasonStats_html += '<td>-</td>'; }
						seasonStats_html += '<td>-</td>';
						seasonStats_html += '<td>' + season_stats[i]['stat']['fielding'] + '</td>';
						seasonStats_html += '</tr>';
					}
				}															 
				seasonStats_html += '<tr><td>MLB Career</td>';
				seasonStats_html += '<td>-</td>';
				seasonStats_html += '<td>-</td>';
				seasonStats_html += '<td>' + career_stats['stat']['position']['abbreviation'] + '</td>';
				seasonStats_html += '<td>' + career_stats['stat']['games'] + '</td>';
				seasonStats_html += '<td>' + career_stats['stat']['gamesStarted'] + '</td>';
				seasonStats_html += '<td>' + career_stats['stat']['innings'] + '</td>';
				seasonStats_html += '<td>' + career_stats['stat']['chances'] + '</td>';
				seasonStats_html += '<td>' + career_stats['stat']['putOuts'] + '</td>';
				seasonStats_html += '<td>' + career_stats['stat']['assists'] + '</td>';
				seasonStats_html += '<td>' + career_stats['stat']['errors'] + '</td>'
				seasonStats_html += '<td>' + career_stats['stat']['doublePlays'] + '</td>';
				if (career_stats['stat']['passedBall']) { seasonStats_html += '<td>' + career_stats['stat']['passedBall'] + '</td>'; } else { seasonStats_html += '<td>-</td>'; }
				if (career_stats['stat']['stolenBases']) { seasonStats_html += '<td>' + career_stats['stat']['stolenBases'] + '</td>'; } else { seasonStats_html += '<td>-</td>'; }
				if (career_stats['stat']['caughtStealing']) { seasonStats_html += '<td>' + career_stats['stat']['caughtStealing'] + '</td>'; } else { seasonStats_html += '<td>-</td>'; }
				seasonStats_html += '<td>-</td>';
				seasonStats_html += '<td>' + career_stats['stat']['fielding'] + '</td>';
				seasonStats_html += '</tr>';
				
				var tableHeadings = '<thead><tr><th>Season</th><th>Team</th><th>LG</th><th>POS</th><th>G</th><th>GS</th><th>INN</th><th>TC</th><th>PO</th><th>A</th><th>E</th><th>DP</th><th>PB</th><th>SB</th><th>CS</th><th>RF</th><th>Fielding %</th></tr></thead>';
				
				this.$(".table-headings.adv").hide();
			}
			this.$("#careerTable").html(tableHeadings+'<tbody>'+seasonStats_html+'</tbody>');
			
			var seasonStatsAdv_html = '';
			var seasonStatsAdv_html2 = '';
			this.$(".table-headings.adv").show();
			this.$("#stats_table_careerAdvanced").show();
			this.$("#stats_table_careerAdvanced2").show();
			if (chart_type == 'pitching'){
				var season_stats = this.model.get('pitching_stats_yearAdv');
				var career_stats = this.model.get('pitching_stats_careerAdv')[0];	
				for (var i = 0; i < season_stats.length; i++) {
					if (season_stats[i]['numTeams']){	// Do not display
					} else {
																 
						var html_tmp = '<tr><td>' + season_stats[i]['season'] + '</td>';
						html_tmp += '<td>' + MLB_teams[season_stats[i]['team']['id']] + '</td>';
						if (season_stats[i]['league']['name'] == 'American League'){
							html_tmp += '<td>AL</td>';
						} else if (season_stats[i]['league']['name'] == 'National League'){
							html_tmp += '<td>NL</td>';
						}
						seasonStatsAdv_html += html_tmp;
						seasonStatsAdv_html2 += html_tmp;
						
						seasonStatsAdv_html += '<td>' + season_stats[i]['stat']['qualityStarts'] + '</td>';
						seasonStatsAdv_html += '<td>' + season_stats[i]['stat']['gamesFinished'] + '</td>';
						seasonStatsAdv_html += '<td>' + season_stats[i]['stat']['doubles'] + '</td>';
						seasonStatsAdv_html += '<td>' + season_stats[i]['stat']['triples'] + '</td>';
						seasonStatsAdv_html += '<td>' + season_stats[i]['stat']['gidp'] + '</td>';
						seasonStatsAdv_html += '<td>' + season_stats[i]['stat']['gidpOpp'] + '</td>';
						seasonStatsAdv_html += '<td>' + season_stats[i]['stat']['wildPitches'] + '</td>';
						seasonStatsAdv_html += '<td>' + season_stats[i]['stat']['balks'] + '</td>';
						seasonStatsAdv_html += '<td>' + season_stats[i]['stat']['stolenBases'] + '</td>';
						seasonStatsAdv_html += '<td>' + season_stats[i]['stat']['caughtStealing'] + '</td>';
						seasonStatsAdv_html += '<td>' + season_stats[i]['stat']['pickoffs'] + '</td>';
						seasonStatsAdv_html += '<td>' + season_stats[i]['stat']['strikePercentage'] + '</td>';
						seasonStatsAdv_html += '<td>' + season_stats[i]['stat']['pitchesPerInning'] + '</td>';
						seasonStatsAdv_html += '<td>' + season_stats[i]['stat']['pitchesPerPlateAppearance'] + '</td>';
						seasonStatsAdv_html += '</tr>';
						
											
						seasonStatsAdv_html2 += '<td>' + season_stats[i]['stat']['winningPercentage'] + '</td>';
						seasonStatsAdv_html2 += '<td>' + season_stats[i]['stat']['runsScoredPer9'] + '</td>';
						seasonStatsAdv_html2 += '<td>' + season_stats[i]['stat']['battersFaced'] + '</td>';
						seasonStatsAdv_html2 += '<td>' + season_stats[i]['stat']['babip'] + '</td>';
						seasonStatsAdv_html2 += '<td>' + season_stats[i]['stat']['obp'] + '</td>';
						seasonStatsAdv_html2 += '<td>' + season_stats[i]['stat']['slg'] + '</td>';
						seasonStatsAdv_html2 += '<td>' + season_stats[i]['stat']['ops'] + '</td>';
						seasonStatsAdv_html2 += '<td>' + season_stats[i]['stat']['strikeoutsPer9'] + '</td>';
						seasonStatsAdv_html2 += '<td>' + season_stats[i]['stat']['baseOnBallsPer9'] + '</td>';
						seasonStatsAdv_html2 += '<td>' + season_stats[i]['stat']['homeRunsPer9'] + '</td>';
						seasonStatsAdv_html2 += '<td>' + season_stats[i]['stat']['hitsPer9'] + '</td>';
						seasonStatsAdv_html2 += '<td>' + season_stats[i]['stat']['strikesoutsToWalks'] + '</td>';
						seasonStatsAdv_html2 += '<td>' + season_stats[i]['stat']['inheritedRunners'] + '</td>';
						seasonStatsAdv_html2 += '<td>' + season_stats[i]['stat']['inheritedRunnersScored'] + '</td>';
						seasonStatsAdv_html2 += '<td>' + season_stats[i]['stat']['bequeathedRunners'] + '</td>';
						seasonStatsAdv_html2 += '<td>' + season_stats[i]['stat']['bequeathedRunnersScored'] + '</td>';
						seasonStatsAdv_html2 += '</tr>';
					}
				}							 
				seasonStatsAdv_html += '<tr><td>MLB Career</td>';
				seasonStatsAdv_html += '<td>-</td>';
				seasonStatsAdv_html += '<td>-</td>';		
								
				seasonStatsAdv_html += '<td>' + career_stats['stat']['qualityStarts'] + '</td>';
				seasonStatsAdv_html += '<td>' + career_stats['stat']['gamesFinished'] + '</td>';
				seasonStatsAdv_html += '<td>' + career_stats['stat']['doubles'] + '</td>';
				seasonStatsAdv_html += '<td>' + career_stats['stat']['triples'] + '</td>';
				seasonStatsAdv_html += '<td>' + career_stats['stat']['gidp'] + '</td>';
				seasonStatsAdv_html += '<td>' + career_stats['stat']['gidpOpp'] + '</td>';
				seasonStatsAdv_html += '<td>' + career_stats['stat']['wildPitches'] + '</td>';
				seasonStatsAdv_html += '<td>' + career_stats['stat']['balks'] + '</td>';
				seasonStatsAdv_html += '<td>' + career_stats['stat']['stolenBases'] + '</td>';
				seasonStatsAdv_html += '<td>' + career_stats['stat']['caughtStealing'] + '</td>';
				seasonStatsAdv_html += '<td>' + career_stats['stat']['pickoffs'] + '</td>';
				seasonStatsAdv_html += '<td>' + career_stats['stat']['strikePercentage'] + '</td>';
				seasonStatsAdv_html += '<td>' + career_stats['stat']['pitchesPerInning'] + '</td>';
				seasonStatsAdv_html += '<td>' + career_stats['stat']['pitchesPerPlateAppearance'] + '</td>';
				seasonStatsAdv_html += '</tr>';
				
											
				seasonStatsAdv_html2 += '<tr><td>MLB Career</td>';
				seasonStatsAdv_html2 += '<td>-</td>';
				seasonStatsAdv_html2 += '<td>-</td>';
				seasonStatsAdv_html2 += '<td>' + career_stats['stat']['winningPercentage'] + '</td>';
				seasonStatsAdv_html2 += '<td>' + career_stats['stat']['runsScoredPer9'] + '</td>';
				seasonStatsAdv_html2 += '<td>' + career_stats['stat']['battersFaced'] + '</td>';
				seasonStatsAdv_html2 += '<td>' + career_stats['stat']['babip'] + '</td>';
				seasonStatsAdv_html2 += '<td>' + career_stats['stat']['obp'] + '</td>';
				seasonStatsAdv_html2 += '<td>' + career_stats['stat']['slg'] + '</td>';
				seasonStatsAdv_html2 += '<td>' + career_stats['stat']['ops'] + '</td>';
				seasonStatsAdv_html2 += '<td>' + career_stats['stat']['strikeoutsPer9'] + '</td>';
				seasonStatsAdv_html2 += '<td>' + career_stats['stat']['baseOnBallsPer9'] + '</td>';
				seasonStatsAdv_html2 += '<td>' + career_stats['stat']['homeRunsPer9'] + '</td>';
				seasonStatsAdv_html2 += '<td>' + career_stats['stat']['hitsPer9'] + '</td>';
				seasonStatsAdv_html2 += '<td>' + career_stats['stat']['strikesoutsToWalks'] + '</td>';
				seasonStatsAdv_html2 += '<td>' + career_stats['stat']['inheritedRunners'] + '</td>';
				seasonStatsAdv_html2 += '<td>' + career_stats['stat']['inheritedRunnersScored'] + '</td>';
				seasonStatsAdv_html2 += '<td>' + career_stats['stat']['bequeathedRunners'] + '</td>';
				seasonStatsAdv_html2 += '<td>' + career_stats['stat']['bequeathedRunnersScored'] + '</td>';
				seasonStatsAdv_html2 += '</tr>';
				
				
				var tableHeadings = '<thead><tr><th>Season</th><th>Team</th><th>LG</th><th>QS</th><th>GF</th><th>2B</th><th>3B</th><th>GIDP</th><th>GIDPO</th><th>WP</th><th>BK</th><th>SB</th><th>CS</th><th>PK</th><th>S%</th><th>P/IP</th><th>P/PA</th></tr></thead>';
				
				var tableHeadings2 = '<thead><tr><th>Season</th><th>Team</th><th>LG</th><th>WPCT</th><th>RS/9</th><th>TBF</th><th>BABIP</th><th>OBP</th><th>SLG</th><th>OPS</th><th>K/9</th><th>BB/9</th><th>HR/9</th><th>H/9</th><th>K/BB</th><th>IR</th><th>IR_S</th><th>BQR</th><th>BQR_S</th></tr></thead>';
				
			} else if (chart_type == 'batting'){
				var season_stats = this.model.get('hitting_stats_yearAdv');
				var career_stats = this.model.get('hitting_stats_careerAdv')[0];	
				for (var i = 0; i < season_stats.length; i++) {
					if (season_stats[i]['numTeams']){	// Do not display
					} else {
																 
						seasonStatsAdv_html += '<tr><td>' + season_stats[i]['season'] + '</td>';
						seasonStatsAdv_html += '<td>' + MLB_teams[season_stats[i]['team']['id']] + '</td>';
						if (season_stats[i]['league']['name'] == 'American League'){
							seasonStatsAdv_html += '<td>AL</td>';
						} else if (season_stats[i]['league']['name'] == 'National League'){
							seasonStatsAdv_html += '<td>NL</td>';
						}
						seasonStatsAdv_html += '<td>' + season_stats[i]['stat']['plateAppearances'] + '</td>';
						seasonStatsAdv_html += '<td>' + season_stats[i]['stat']['totalBases'] + '</td>';
						seasonStatsAdv_html += '<td>' + season_stats[i]['stat']['extraBaseHits'] + '</td>';
						seasonStatsAdv_html += '<td>' + season_stats[i]['stat']['hitByPitch'] + '</td>';
						seasonStatsAdv_html += '<td>' + season_stats[i]['stat']['sacBunts'] + '</td>';
						seasonStatsAdv_html += '<td>' + season_stats[i]['stat']['sacFlies'] + '</td>';
						seasonStatsAdv_html += '<td>' + season_stats[i]['stat']['babip'] + '</td>';
						seasonStatsAdv_html += '<td>' + season_stats[i]['stat']['gidp'] + '</td>';
						seasonStatsAdv_html += '<td>' + season_stats[i]['stat']['gidpOpp'] + '</td>';
						seasonStatsAdv_html += '<td>' + season_stats[i]['stat']['numberOfPitches'] + '</td>';
						seasonStatsAdv_html += '<td>' + season_stats[i]['stat']['pitchesPerPlateAppearance'] + '</td>';
						seasonStatsAdv_html += '<td>' + season_stats[i]['stat']['reachedOnError'] + '</td>';
						seasonStatsAdv_html += '<td>' + season_stats[i]['stat']['leftOnBase'] + '</td>';
						seasonStatsAdv_html += '<td>' + season_stats[i]['stat']['walkOffs'] + '</td>';
						seasonStatsAdv_html += '</tr>';
					}
				}															 
				seasonStatsAdv_html += '<tr><td>MLB Career</td>';
				seasonStatsAdv_html += '<td>-</td>';
				seasonStatsAdv_html += '<td>-</td>';
				seasonStatsAdv_html += '<td>' + career_stats['stat']['plateAppearances'] + '</td>';
				seasonStatsAdv_html += '<td>' + career_stats['stat']['totalBases'] + '</td>';
				seasonStatsAdv_html += '<td>' + career_stats['stat']['extraBaseHits'] + '</td>';
				seasonStatsAdv_html += '<td>' + career_stats['stat']['hitByPitch'] + '</td>';
				seasonStatsAdv_html += '<td>' + career_stats['stat']['sacBunts'] + '</td>';
				seasonStatsAdv_html += '<td>' + career_stats['stat']['sacFlies'] + '</td>';
				seasonStatsAdv_html += '<td>' + career_stats['stat']['babip'] + '</td>';
				seasonStatsAdv_html += '<td>' + career_stats['stat']['gidp'] + '</td>';
				seasonStatsAdv_html += '<td>' + career_stats['stat']['gidpOpp'] + '</td>';
				seasonStatsAdv_html += '<td>' + career_stats['stat']['numberOfPitches'] + '</td>';
				seasonStatsAdv_html += '<td>' + career_stats['stat']['pitchesPerPlateAppearance'] + '</td>';
				seasonStatsAdv_html += '<td>' + career_stats['stat']['reachedOnError'] + '</td>';
				seasonStatsAdv_html += '<td>' + career_stats['stat']['leftOnBase'] + '</td>';
				seasonStatsAdv_html += '<td>' + career_stats['stat']['walkOffs'] + '</td>';
				seasonStatsAdv_html += '</tr>';
				
				var tableHeadings = '<thead><tr><th>Season</th><th>Team</th><th>LG</th><th>PA</th><th>TB</th><th>XBH</th><th>HBP</th><th>SAC</th><th>SF</th><th>BABIP</th><th>GIDP</th><th>GIDPO</th><th>NP</th><th>P/PA</th><th>ROE</th><th>LOB</th><th>WO</th></tr></thead>';			
			} else if (chart_type = 'fielding'){
				this.$(".table-headings.adv").hide();
				this.$("#stats_table_careerAdvanced").hide();
				this.$("#stats_table_careerAdvanced2").hide();
			}
			this.$("#careerAdvancedTable").html(tableHeadings+'<tbody>'+seasonStatsAdv_html+'</tbody>');
			if (seasonStatsAdv_html2){
				this.$("#careerAdvancedTable2").html(tableHeadings2+'<tbody>'+seasonStatsAdv_html2+'</tbody>');
			}
			
		},
		
		activeClicked:function (event) {
      		event.preventDefault();			
			$("#nav_40").removeClass('tab_current');
			$("#nav_coaches").removeClass('tab_current');
			$("#nav_active").addClass('tab_current');
			$('.roster_table_container').css('opacity', 0.5);
			var self = this;
			var teamPage = new Page();
			teamPage.url = "http://localhost/wordpress-5.2.2/wp-json/baseball/team/" + this.options.teamCode;
			teamPage.fetch({
				success: function (data, response) {
					self.$('.roster_table_container').css('opacity', 1);
					self.refreshTable(response);
				}
			});
		},		
		
		fortyClicked:function (event) {
      		event.preventDefault();
			$("#nav_active").removeClass('tab_current');
			$("#nav_coaches").removeClass('tab_current');
			$("#nav_40").addClass('tab_current');
			$('.roster_table_container').css('opacity', 0.5);
			var self = this;
			var teamPage = new Page();
			teamPage.url = "http://localhost/wordpress-5.2.2/wp-json/baseball/team/"+this.options.teamCode+'/40man';
			teamPage.fetch({
				success: function (data, response) {
					self.$('.roster_table_container').css('opacity', 1);
					self.refreshTable(response);
				}
			});
			
		},		
		
		coachesClicked:function (event) {
      		event.preventDefault();
			$("#nav_active").removeClass('tab_current');
			$("#nav_40").removeClass('tab_current');
			$("#nav_coaches").addClass('tab_current');
			$('.roster_table_container').css('opacity', 0.5);
			var self = this;
			var teamPage = new Page();
			teamPage.url = "http://localhost/wordpress-5.2.2/wp-json/baseball/team/"+this.options.teamCode+'/coaches';
			teamPage.fetch({
				success: function (data, response) {
					self.$('.roster_table_container').css('opacity', 1);
					self.refreshTable(response);
				}
			});
		},
		
		pitchingClicked:function (event) {
      		event.preventDefault();
			$("#nav_batting").removeClass('tab_current');
			$("#nav_fielding").removeClass('tab_current');
			$("#nav_pitching").addClass('tab_current');

			this.model.set('table_mode', 'pitching');
			this.renderTable();
			
		},
		
		battingClicked:function (event) {
      		event.preventDefault();
			$("#nav_pitching").removeClass('tab_current');
			$("#nav_fielding").removeClass('tab_current');
			$("#nav_batting").addClass('tab_current');
			
			this.model.set('table_mode', 'batting');
			this.renderTable(this.model.toJSON());
			
		},
		
		fieldingClicked:function (event) {
      		event.preventDefault();
			$("#nav_pitching").removeClass('tab_current');
			$("#nav_batting").removeClass('tab_current');
			$("#nav_fielding").addClass('tab_current');

			this.model.set('table_mode', 'fielding');
			this.renderTable(this.model.toJSON());
			
		},
		
		refreshTable:function (response) {
			team_roster = response.roster;
			var self = this;
			var innerTable_pitchers = '';
			var innerTable_catchers = '';
			var innerTable_infield = '';
			var innerTable_outfield = '';
			var innerTable_coaches = '';
			var isPlayer = team_roster[0]['isPlayer'];
			
			if (team_roster.length > 30){
				this.$(".legend").show();
			} else {
				this.$(".legend").hide();
			}
			for (var i = 0; i < team_roster.length; i++) {
				var innerTable = '';
				innerTable += '<tr id="player-' + team_roster[i]['id'] + '">';
				if (team_roster[i]['jerseyNumber']){
					innerTable += '<td>' + team_roster[i]['jerseyNumber'] + '</td>';
				} else {
					innerTable += '<td>-</td>';
				}
				//http://gdx.mlb.com/images/gameday/mugshots/mlb/592094@2x.jpg
				//https://securea.mlb.com/mlb/images/players/head_shot/ID.jpg
				//http://mlb.mlb.com/images/coaches/mugshots/124x150/
				
				if (isPlayer == 1){
					var tmp_name = team_roster[i]['name'];
					var url_name = tmp_name.replace(/[^a-z0-9]/gi, '-').toLowerCase();
					var player_link = '#player/' + team_roster[i]['id']+'/'+url_name;
					
					innerTable += '<td><div class="player-headshot"><img src="http://gdx.mlb.com/images/gameday/mugshots/mlb/'+team_roster[i]['id']+'@2x.jpg" /></div></td>';
					innerTable += '<td><a href="'+player_link+'">' + team_roster[i]['name'] + '</a>';
					
					if (team_roster[i]['status_code'] != 'A'){
						if (team_roster[i]['status_code'] == 'D10'){
							innerTable += ' <span class="star">*</span><br />(' + team_roster[i]['status_description']+')';
						} else if (team_roster[i]['status_code'] == 'D60'){
							innerTable += ' <span class="star">**</span><br />(' + team_roster[i]['status_description']+')';
						} else {
							innerTable += ' <span class="star">*</span>';
						}
					}
					innerTable += '</td>';
					
					innerTable += '<td>' + team_roster[i]['player']['batSide_code']+'/'+team_roster[i]['player']['pitchHand_code'] + '</td>';
					innerTable += '<td>' + team_roster[i]['player']['height'] + '</td>';
					innerTable += '<td>' + team_roster[i]['player']['weight'] + 'lbs</td>';
					innerTable += '<td>' + team_roster[i]['player']['birthDate'] + '</td>';
					innerTable += '</tr>';
					
					if (team_roster[i]['position']['type'] == 'Pitcher'){
						innerTable_pitchers += innerTable;
					} else if (team_roster[i]['position']['type'] == 'Catcher'){
						innerTable_catchers += innerTable;
					} else if (team_roster[i]['position']['type'] == 'Infielder'){
						innerTable_infield += innerTable;
					} else if (team_roster[i]['position']['type'] == 'Outfielder'){
						innerTable_outfield += innerTable;
					}
				} else {
					innerTable += '<td><div class="player-headshot"><img src="http://mlb.mlb.com/images/coaches/mugshots/124x150/'+team_roster[i]['id']+'.jpg" /></div></td>';
					innerTable += '<td>' + team_roster[i]['name'] + '</td>';
					
					innerTable += '<td>' + team_roster[i]['job'] + '</td>';
					innerTable_coaches += innerTable;
				}
			}
			
			if (isPlayer == 1){			
				var tableHeadings = '<tr class="headings"><td>#</td><td></td><td>Name</td><td>B/T</td><td>Ht</td><td>Wt</td><td>DOB</td></tr></thead>';
				this.$("#pTable").html('<thead><tr><th colspan="7">Pitchers</th></tr>'+tableHeadings+'<tbody>'+innerTable_pitchers+'</tbody>');
				this.$("#cTable").html('<thead><tr><th colspan="7">Catchers</th></tr>'+tableHeadings+'<tbody>'+innerTable_catchers+'</tbody>');	
				this.$("#iTable").html('<thead><tr><th colspan="7">Infield</th></tr>'+tableHeadings+'<tbody>'+innerTable_infield+'</tbody>');	
				this.$("#oTable").html('<thead><tr><th colspan="7">Outfield</th></tr>'+tableHeadings+'<tbody>'+innerTable_outfield+'</tbody>');	
				this.$("#pTable").show();
				this.$("#cTable").show();
				this.$("#iTable").show();
				this.$("#oTable").show();
				this.$("#coachesTable").hide();
			} else {
				var tableHeadings = '<tr class="headings"><td>#</td><td></td><td>Name</td><td>Position</td></tr></thead>';
				this.$("#coachesTable").html('<thead><tr><th colspan="7">Manager & Coaches</th></tr>'+tableHeadings+'<tbody>'+innerTable_coaches+'</tbody>');	
				this.$("#coachesTable").show();
				this.$("#pTable").hide();
				this.$("#cTable").hide();
				this.$("#iTable").hide();
				this.$("#oTable").hide();
			}	
			
		},
		
		refreshView:function () {
			var self = this;
			if (this.options.viewType == "team"){
				$(this.el).html(this.template({viewType: this.options.viewType}));	
			
				var team_roster = this.model.get('roster');
				
				var innerTable_pitchers = '';
				var innerTable_catchers = '';
				var innerTable_infield = '';
				var innerTable_outfield = '';
				for (var i = 0; i < team_roster.length; i++) {
					var innerTable = '';
					innerTable += '<tr id="player-' + team_roster[i]['id'] + '">';
					innerTable += '<td>' + team_roster[i]['jerseyNumber'] + '</td>';
					
					var tmp_name = team_roster[i]['name'];
					var url_name = tmp_name.replace(/[^a-z0-9]/gi, '-').toLowerCase();
					var player_link = '#player/' + team_roster[i]['id']+'/'+url_name;
					//http://gdx.mlb.com/images/gameday/mugshots/mlb/592094@2x.jpg
					//https://securea.mlb.com/mlb/images/players/head_shot/ID.jpg
					innerTable += '<td><div class="player-headshot"><img src="http://gdx.mlb.com/images/gameday/mugshots/mlb/'+team_roster[i]['id']+'@2x.jpg" /></div></td>';
					innerTable += '<td><a href="'+player_link+'">' + team_roster[i]['name'] + '</a></td>';
					
					innerTable += '<td>' + team_roster[i]['player']['batSide_code']+'/'+team_roster[i]['player']['pitchHand_code'] + '</td>';
					innerTable += '<td>' + team_roster[i]['player']['height'] + '</td>';
					innerTable += '<td>' + team_roster[i]['player']['weight'] + 'lbs</td>';
					innerTable += '<td>' + team_roster[i]['player']['birthDate'] + '</td>';
					
					innerTable += '</tr>';
					
					if (team_roster[i]['position']['type'] == 'Pitcher'){
						innerTable_pitchers += innerTable;
					} else if (team_roster[i]['position']['type'] == 'Catcher'){
						innerTable_catchers += innerTable;
					} else if (team_roster[i]['position']['type'] == 'Infielder'){
						innerTable_infield += innerTable;
					} else if (team_roster[i]['position']['type'] == 'Outfielder'){
						innerTable_outfield += innerTable;
					}
				}
				
				var tableHeadings = '<tr class="headings"><td>#</td><td></td><td>Name</td><td>B/T</td><td>Ht</td><td>Wt</td><td>DOB</td></tr></thead>';
				this.$("#pTable").html('<thead><tr><th colspan="7">Pitchers</th></tr>'+tableHeadings+'<tbody>'+innerTable_pitchers+'</tbody>');	
				this.$("#cTable").html('<thead><tr><th colspan="7">Catchers</th></tr>'+tableHeadings+'<tbody>'+innerTable_catchers+'</tbody>');	
				this.$("#iTable").html('<thead><tr><th colspan="7">Infield</th></tr>'+tableHeadings+'<tbody>'+innerTable_infield+'</tbody>');	
				this.$("#oTable").html('<thead><tr><th colspan="7">Outfield</th></tr>'+tableHeadings+'<tbody>'+innerTable_outfield+'</tbody>');	
					
			}
		}			
	});
});
