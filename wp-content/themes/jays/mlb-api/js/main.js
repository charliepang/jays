jQuery(function ($) {
	// Script for allowing IE to display page properly (console being undefined was an issue before)
	if (!window.console) (function() {
		var __console, Console;
		Console = function() {
			var check = setInterval(function() {
				var f;
				if (window.console && console.log && !console.__buffer) {
					clearInterval(check);
					f = (Function.prototype.bind) ? Function.prototype.bind.call(console.log, console) : console.log;
					for (var i = 0; i < __console.__buffer.length; i++) f.apply(console, __console.__buffer[i]);
				}
			}, 1000); 
	
			function log() {
				this.__buffer.push(arguments);
			}
	
			this.log = log;
			this.error = log;
			this.warn = log;
			this.info = log;
			this.__buffer = [];
		};
		__console = window.console = new Console();
	})();
	$.ajaxSetup({ cache: false });
	
	
	// Tell jQuery to watch for any 401 or 403 errors and handle them appropriately
	$.ajaxSetup({
		statusCode: {
			401: function(){
				// Redirec the to the login page.
				window.location.replace('#login');
			 
			},
			403: function() {
				// 403 -- Access denied
				window.location.replace('#denied');
			}
		},
		cache: false
	});
	
	//window.Router = Backbone.Router.extend({
	var Router = Backbone.Router.extend({
	
		routes: {
			"": "home",
			"home": "home",
			
			"teams": "teams",
			"american_league": "teams_al",
			"national_league": "teams_nl",
			"team/:team_code": "team",
			"player/:player_id/:player_name": "player"
			
		},
		
		home: function() {	
			$("html, body").animate({ scrollTop: 0 }, 350);
			$('header#masthead .home-header').show();
			$('header#masthead .reg-header').hide();
			$('#content').hide();
			
			$('#portal-content').html(new MiscView({viewType: "home"}).el);
		},
		
		teams: function() {	
			$("html, body").animate({ scrollTop: 0 }, 350);
			$('header#masthead .home-header').hide();
			$('header#masthead .reg-header').show();
			$('#content').show();
			
			var page = new Page();
			page.url = "http://localhost/wordpress-5.2.2/wp-json/baseball/teams";
			page.fetch({
				success: function (data, response) {
					$('#portal-content').html(new MiscView({model: data, viewType: "teams"}).el);
				}
			});
		},
		
		teams_al: function() {	
			$("html, body").animate({ scrollTop: 0 }, 350);
			$('header#masthead .home-header').hide();
			$('header#masthead .reg-header').show();
			$('#content').show();
			
			var page = new Page();
			page.url = "http://localhost/wordpress-5.2.2/wp-json/baseball/teams";
			page.fetch({
				success: function (data, response) {
					$('#portal-content').html(new MiscView({model: data, viewType: "teams_al"}).el);
				}
			});
		},
		
		teams_nl: function() {	
			$("html, body").animate({ scrollTop: 0 }, 350);
			$('header#masthead .home-header').hide();
			$('header#masthead .reg-header').show();
			$('#content').show();
			
			var page = new Page();
			page.url = "http://localhost/wordpress-5.2.2/wp-json/baseball/teams";
			page.fetch({
				success: function (data, response) {
					$('#portal-content').html(new MiscView({model: data, viewType: "teams_nl"}).el);
				}
			});
		},
					
		team: function(team_code) {
			$("html, body").animate({ scrollTop: 0 }, 350);
			$('header#masthead .home-header').hide();
			$('header#masthead .reg-header').show();
			$('#content').show();
			
			var teamPage = new Page();
			teamPage.url = "http://localhost/wordpress-5.2.2/wp-json/baseball/team/" + team_code;
			teamPage.fetch({
				success: function (data, response) {
					$('#portal-content').html(new MiscView({model: data, viewType: "team", teamCode: team_code, teamID: response.id}).el);
				}
			});
		},	
		
		player: function(player_id, player_name) {
			$("html, body").animate({ scrollTop: 0 }, 350);
			$('header#masthead .home-header').hide();
			$('header#masthead .reg-header').show();
			$('#content').show();
			
			var playerPage = new Page();
			playerPage.url = "http://localhost/wordpress-5.2.2/wp-json/baseball/player/" + player_id;
			playerPage.fetch({
				success: function (data, response) {
					$('#portal-content').html(new MiscView({model: data, viewType: "player", player_id: response.id}).el);
				}
			});
		}
		
		
		// ==========================================================================================================================
	
	});
	templateLoader.load(["MiscView"],
		function () {
			var app_router = new Router();
			Backbone.history.start();  
		});
});
