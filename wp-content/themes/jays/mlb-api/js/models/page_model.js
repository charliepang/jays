window.Page = Backbone.Model.extend({

    initialize:function (id) {
		this.validators = {};
    },

    validateItem: function (key) {
        return (this.validators[key]) ? this.validators[key](this.get(key)) : {isValid: true};
    },

    validateAll: function () {
        var messages = {};
        for (var key in this.validators) {
            if(this.validators.hasOwnProperty(key)) {
				if (key == 'province'){
                	var check = this.validators[key](this.get(key), this.get('country'));
				} else {
                	var check = this.validators[key](this.get(key));
				}
                if (check.isValid === false) {
                    messages[key] = check.message;
                }
            }
		//alert('kA: ' + check.message);
		//alert('kB: ' + messages);
        }
        return _.size(messages) > 0 ? {isValid: false, messages: messages} : {isValid: true};
    }
});

window.PageCollection = Backbone.Collection.extend({
    model: Page
});