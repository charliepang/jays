	
jQuery(function ($) {
	// The Template Loader. Used to asynchronously load templates located in separate .html files
	window.templateLoader = {
	
		load: function(views, callback) {
	
			var deferreds = [];
	
			$.each(views, function(index, view) {
				if (window[view]) {
					deferreds.push($.get('http://localhost/wordpress-5.2.2/wp-content/themes/jays/mlb-api/tpl/' + view + '.php', function(data) {
						window[view].prototype.template = _.template(data);
					}, 'html'));
				} else {
					alert(view + " not found");
				}
			});
	
			$.when.apply(null, deferreds).done(callback);
		}
	
	};
	

 });