<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Nineteen
 * @since 1.0.0
 */
?><!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<link rel="profile" href="https://gmpg.org/xfn/11" />
	<?php wp_head(); ?>
    
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/bootstrap.css">    
    <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
</head>

<body <?php body_class(); ?> data-spy="scroll" data-target="#mainNav" data-offset="50">
<?php wp_body_open(); ?>
<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#content"><?php _e( 'Skip to content', 'twentynineteen' ); ?></a>
    
		<header id="masthead" class="<?php echo is_singular() && twentynineteen_can_show_post_thumbnail() ? 'site-header featured-image' : 'site-header'; ?>">
        	<div class="home-header">
    			<?php include 'header.inc.php';?>
            	
                <div class="intro-text">
                    <p class="heading1"><strong>Welcome to Charlie Pang's website</strong></p>
                    <p class="heading2">Team and Player Stats Application</p>
                </div>
            </div>
            
			<div class="reg-header">
    			<?php include 'header.inc.php';?>
            </div>
		</header><!-- #masthead -->
        

	<div id="content" class="site-content">
