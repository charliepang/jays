

<?php
/**
 * Template Name: Portal
 *
 *
 * @package WordPress
 * @subpackage Twenty_Nineteen
 * @since 1.0.0
 */

get_header();
?>
	<style type="text/css">
    </style>

	<section id="primary" class="content-area">
		<main id="main" class="site-main">			
            
            <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                <header class="entry-header">
                	<h1 class="entry-title">Title</h1>
                </header><!-- .entry-header -->
                        
                <div class="entry-content">
        			<div id="portal-content"><br /><br /></div>
                </div><!-- .entry-content -->
            
                <footer class="entry-footer">
                </footer><!-- .entry-footer -->
            </article><!-- #post-<?php the_ID(); ?> -->


		</main><!-- #main -->
	</section><!-- #primary -->

<?php
get_footer();
