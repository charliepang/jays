<?php
/**
 * Template Name: Portal
 *
 * Description: Twenty Twelve loves the no-sidebar look as much as
 * you do. Use this page template to remove the sidebar from any page.
 *
 * Tip: to remove the sidebar from all posts and pages simply remove
 * any active widgets from the Main Sidebar area, and the sidebar will
 * disappear everywhere.
 *
 * @package WordPress
 * @subpackage Twenty_Twelve
 * @since Twenty Twelve 1.0
 */

get_header(); ?>
	<style type="text/css">
    </style>
  <div class="boxgl">
        <div class="portal-header"></div>
        
		<?php /* 
		<script src="<?php echo get_template_directory_uri(); ?>/js/underscore.js"></script>
		<script src="<?php echo get_template_directory_uri(); ?>/js/backbone.js"></script> */ ?>
        
        
        <div id="portal-content"><br /><br /></div>
        
        <?php //do_shortcode('[WP-BACKBONE]'); ?>
	</div>
<?php get_footer(); ?>